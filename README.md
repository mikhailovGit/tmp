```mermaid
sequenceDiagram
    participant ...
    participant App1
    participant App2
    participant Quene
    participant App3
    ...->>App1:Get App1/
    App1->>App2: Post 'DtoExample'
    App2->>Quene: MasstransitEventExample
    Quene->>App3: MasstransitEventExample
    App3->>App3: throw new Excpetion("AAAAAaaa!!")
```

```mermaid
graph TD
    classDef event fill:#f96;

    Initial
    WaitPayment
    Final
    OrderCompleted
    SubmitOrder
    WaitPaymentResponded
    Initial --> SubmitOrder
    SubmitOrder --> WaitPayment
    WaitPayment --> WaitPaymentResponded
    WaitPaymentResponded --> Final
    WaitPaymentResponded --> OrderCompleted
```



## Urls

| Service               | Port                                        |
|-----------------------|---------------------------------------------|
| App1                  | http://82.146.63.92:5001/swagger/index.html |
| App2                  | http://82.146.63.92:5002/swagger/index.html |
| App3                  | http://82.146.63.92:5003                    |
| rabbitmq              | http://82.146.63.92:15672                    |
| grafana               | http://82.146.63.92:3000                     |


