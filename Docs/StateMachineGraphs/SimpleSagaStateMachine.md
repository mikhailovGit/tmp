```mermaid
graph TD
    classDef event fill:#f96;

    Initial
    WaitPayment
    Final
    OrderCompleted
    SubmitOrder
    WaitPaymentResponded
    Initial --> SubmitOrder
    SubmitOrder --> WaitPayment
    WaitPayment --> WaitPaymentResponded
    WaitPaymentResponded --> Final
    WaitPaymentResponded --> OrderCompleted

