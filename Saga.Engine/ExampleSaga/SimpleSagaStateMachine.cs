﻿using Common.Enums;
using Common.Masstransit.Events;
using MassTransit;

namespace Saga.Engine.ExampleSaga;

// [DatabaseSchemaName("saga")] 
public class SimpleSagaStateMachine : MassTransitStateMachine<SimpleSagaState>
{
    public SimpleSagaStateMachine()
    {
        Event(() => SubmitOrder, x => x.CorrelateById(context => context.Message.CorrelationId));
        Event(() => SubmitOrderResponded, x => x.CorrelateById(context => context.Message.CorrelationId));
        Event(() => WaitPaymentResponded, x => x.CorrelateById(context => context.Message.CorrelationId));

        InstanceState(x => x.CurrentState);

        Initially(
            When(SubmitOrder)
                .Then(
                    context =>
                    {
                        context.Saga.CorrelationId = context.Saga.CorrelationId;
                        context.Saga.OrderId = context.Saga.OrderId;
                        context.Saga.OrderStatus = context.Saga.OrderStatus;
                    })
                .Publish(
                    context => new WithdrawMoney.Requested
                    {
                        CorrelationId = context.Saga.CorrelationId,
                        OrderId = context.Saga.OrderId,
                        OrderStatus = context.Saga.OrderStatus
                    })
                .TransitionTo(WaitPayment)
        );

        During(
            WaitPayment,
            When(WaitPaymentResponded)
                .Then(
                    context =>
                    {
                        context.Saga.CorrelationId = context.Saga.CorrelationId;
                        context.Saga.OrderId = context.Saga.OrderId;
                        context.Saga.OrderStatus = context.Saga.OrderStatus;
                    })
                .IfElse(
                    _ => _.Saga.OrderStatus == OrderStatus.Completed,
                    binder => binder.Publish(
                            context => new OrderCompleted.Raised
                            {
                                CorrelationId = context.Message.CorrelationId,
                                OrderId = context.Saga.OrderId,
                                OrderStatus = context.Saga.OrderStatus
                            })
                        .Finalize(),
                    binder => binder.TransitionTo(OrderCompleted)
                )
        );
    }

    public State WaitPayment { get; set; } = null!;

    public State OrderCompleted { get; set; } = null!;

    public Event<SubmitOrder.Requested> SubmitOrder { get; } = null!;
    public Event<SubmitOrder.Responded> SubmitOrderResponded { get; } = null!;

    public Event<WithdrawMoney.Responded> WaitPaymentResponded { get; } = null!;
}
