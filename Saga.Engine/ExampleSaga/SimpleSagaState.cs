﻿using Common.Enums;
using MassTransit;

namespace Saga.Engine.ExampleSaga;

// [DatabaseSchemaName("saga")]  Marten 
public class SimpleSagaState : SagaStateMachineInstance
{
    public string? CurrentState { get; set; }
    public OrderStatus OrderStatus { get; set; }
    public Guid OrderId { get; set; }
    public Guid CorrelationId { get; set; }
}
