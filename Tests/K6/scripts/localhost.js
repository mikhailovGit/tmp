import http from 'k6/http';
import {check, sleep} from "k6";

export let options = {
    stages: [
        // { duration: "2m", target: 100 },
        {duration: "5m", target: 3000},
        {duration: "2m", target: 0}
    ]
};

export default function () {
    const response = http.get("http://mkv.sytes.net/app1/App1?someString=DDDDDDDDDDD", {headers: {Accepts: "application/json"}});
    check(response, {"status is 200": (r) => r.status === 200});
    sleep(.300);
};
