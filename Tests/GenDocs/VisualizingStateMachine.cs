using System.Runtime.CompilerServices;
using System.Text;
using Automatonymous;
using Automatonymous.Visualizer;
using MassTransit.SagaStateMachine;
using Saga.Engine.ExampleSaga;
using Xunit.Abstractions;

namespace GenDocs;

public class VisualizingStateMachine
{
//     private const string Header = @"---
// mermaid: true
// ---
// {{< mermaid >}}";
//
//     private const string Footer = "{{< /mermaid >}}";

    private const string Header = @"```mermaid";
    private const string Footer = "";

    private readonly ITestOutputHelper _testOutputHelper;

    public VisualizingStateMachine(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    public static IEnumerable<object[]> StateMachineFactoryData =>
        new[]
        {
            new object[] { () => new SimpleSagaStateMachine() }
        };

    [Fact]
    // [MemberData(nameof(StateMachineFactoryData))]
    public void GenerateStateMachineGraph()
    {
        var machine = new SimpleSagaStateMachine();
        var graph = machine.GetGraph();
        var dots = GenerateMermaidGraph(graph);

        var repositoryRoot = GetRepositoryRoot();

        _testOutputHelper.WriteLine(dots);

        var path = Path.Combine(repositoryRoot, "Docs", "StateMachineGraphs", $"{nameof(SimpleSagaStateMachine)}.md");
        File.WriteAllText(path, @$"{Header}
{dots}
{Footer}");
    }

    private static string GetRepositoryRoot([CallerFilePath] string? callerFilePath = null)
    {
        return Directory.GetParent(callerFilePath ?? "")?.Parent?.Parent?.FullName ?? "";
    }

    private static string GenerateMermaidGraph(StateMachineGraph graph)
    {
        var stringBuilder = new StringBuilder();
        stringBuilder.AppendLine("graph TD");
        stringBuilder.AppendLine("    classDef event fill:#f96;");
        stringBuilder.AppendLine();
        foreach (var vertex in graph.Vertices)
        {
            stringBuilder.Append("    ").Append(vertex.Title);
            if (vertex.VertexType == typeof(Event))
            {
                stringBuilder.Append($"[/{vertex.Title}/]:::event");
            }

            stringBuilder.AppendLine();
        }

        foreach (var edge in graph.Edges)
        {
            stringBuilder
                .Append("    ")
                .Append(edge.From.Title)
                .Append(" --> ")
                .AppendLine(edge.To.Title);
        }

        return stringBuilder.ToString();
    }

    // ReSharper disable once UnusedMember.Local (Sample from Automatonymous.Visualizer)
    private static string Dots(Automatonymous.Graphing.StateMachineGraph graph)
    {
        var generator = new StateMachineGraphvizGenerator(graph);
        var dots = generator.CreateDotFile();
        return dots;
    }
}
