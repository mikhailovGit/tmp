﻿using System.ComponentModel.DataAnnotations;

namespace Dal;

public class TestTable
{
    [Key]
    public Guid Id { get; set; }
    
    public string TestProp { get; set; } = null!;
}