﻿using System.Data.Entity;

namespace Dal;

// [DbConfigurationType("Prometheus.EF.PrometheusEF6CodeConfig, Prometheus.EF")]
public partial class MyDbContext : DbContext
{
    public DbSet<TestTable> TestTables { get; set; } = null!;
}       