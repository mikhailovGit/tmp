﻿using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTelemetry.Exporter;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace Common.Telemetry;

public static class ServiceCollectionExtensions
{
    public static void AddLogging(this WebApplicationBuilder builder)
    {
        builder.Services.AddHttpLogging(httpLogging =>
        {
            httpLogging.LoggingFields = HttpLoggingFields.All;

            httpLogging.MediaTypeOptions.AddText("application/javascript");
        });
    }

    public static void AddTracing(this WebApplicationBuilder builder, string serviceName)
    {
        var collectorUrl = new Uri("http://collector:4317");

        builder.Services.AddOpenTelemetryTracing(x =>
        {
            x.SetResourceBuilder(ResourceBuilder.CreateDefault()
                .AddService(serviceName)
                .AddTelemetrySdk()
                .AddEnvironmentVariableDetector());

            x.AddSource("MassTransit");

            x.AddAspNetCoreInstrumentation(options => { options.Enrich = EnrichActivity; });
            x.AddHttpClientInstrumentation(options => { options.Enrich = EnrichActivity; });

            x.AddOtlpExporter(options => { options.Endpoint = collectorUrl; });

            x.AddZipkinExporter();
            x.AddJaegerExporter();
        });

        builder.Logging.AddOpenTelemetry(x =>
        {
            x.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName));
            x.IncludeFormattedMessage = true;
            x.IncludeScopes = true;
            x.ParseStateValues = true;

            x.AddOtlpExporter(options => { options.Endpoint = collectorUrl; });
            x.AddConsoleExporter();
        });
    }

    private static void EnrichActivity(Activity activity, string key, object value)
    {
        var tagValue = Guid.NewGuid() + "111111";

        activity.AddAddBaggageIfNotExist(TelemetryConstants.Some, tagValue);
        activity.AddTagIfNotExist(TelemetryConstants.Some, activity.GetBaggageItem(TelemetryConstants.Some));
    }
}
