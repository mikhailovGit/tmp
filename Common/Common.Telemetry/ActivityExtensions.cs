﻿using System.Diagnostics;

namespace Common.Telemetry;

public static class ActivityExtensions
{
    public static void AddTagIfNotExist(this Activity? activity, string? tagName, string? tagValue)
    {
        if (tagName == null)
        {
            throw new ArgumentNullException(nameof(tagName));
        }

        if (activity != null && activity.GetTagItem(tagName) == default)
        {
            activity.AddTag(tagName, tagValue);
        }
    }

    public static void AddAddBaggageIfNotExist(this Activity? activity, string baggageName, string? tagValue)
    {
        if (baggageName == null)
        {
            throw new ArgumentNullException(nameof(baggageName));
        }

        if (activity != null && activity.GetBaggageItem(baggageName) == default)
        {
            activity.AddBaggage(baggageName, tagValue);
        }
    }
}
