﻿using Common.Masstransit.Observers;
using Common.Masstransit.Tracker;
using Common.Settings;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Saga.Host;

namespace Common.Host.Extensions;

public static class MassTransitBusExtension
{
    public static void AddMasstransit(
        this WebApplicationBuilder builder,
        Action<IBusRegistrationConfigurator>? configure = null)
    {
        var rabbitMqSettings = new RabbitMqSettings();
        builder.Configuration.GetSection(nameof(RabbitMqSettings)).Bind(rabbitMqSettings);

        builder.AddMassTransitObserver();

        builder.Services.AddMassTransit(configurator =>
        {
            configure?.Invoke(configurator);

            configurator.UsingRabbitMq((context, cfg) =>
            {
                cfg.UseInMemoryOutbox();
                cfg.Host(rabbitMqSettings.Host, "/", h =>
                {
                    h.Username(rabbitMqSettings.Username);
                    h.Password(rabbitMqSettings.Password);
                });

                cfg.ConfigureEndpoints(context);
                cfg.AddTrackerFilters(context);
                cfg.ConnectMassTransitObserver(context);
            });

            configurator.AddSagas();
        });
    }
}
