﻿namespace Common.Enums;

public enum OrderStatus
{
    None = 0,
    Submitted = 1,
    WithdrawMoney = 2,
    Closed,
    Failed,
    Completed
}
