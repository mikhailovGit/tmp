﻿using Common.Enums;

namespace Common.Masstransit.Events;

public class WithdrawMoney
{
    public Guid CorrelationId { get; set; }

    public Guid OrderId { get; set; }
    public OrderStatus OrderStatus { get; set; }
    
    public sealed class Requested : WithdrawMoney
    {
    }

    public sealed class Responded : WithdrawMoney
    {
    }
}
