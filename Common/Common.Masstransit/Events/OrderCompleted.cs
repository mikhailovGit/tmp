﻿using Common.Enums;

namespace Common.Masstransit.Events;

public class OrderCompleted
{
    public sealed class Raised
    {
        public Guid CorrelationId { get; set; }
        public Guid OrderId { get; set; }
        public OrderStatus OrderStatus { get; set; }
    }
}
