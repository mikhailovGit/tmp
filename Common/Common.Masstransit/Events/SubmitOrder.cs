﻿using Common.Enums;

namespace Common.Masstransit.Events;

public class SubmitOrder
{
    public Guid CorrelationId { get; set; }
    public Guid OrderId { get; set; }
    public OrderStatus OrderStatus { get; set; }

    public sealed class Requested : SubmitOrder
    {
    }

    public sealed class Responded : SubmitOrder
    {
    }
}
