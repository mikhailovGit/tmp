﻿using System.Diagnostics;
using Common.Telemetry;
using MassTransit;

namespace Common.Masstransit.Tracker.Filters;

public static class HeaderTrackFilterExtension
{
    public static void SetAllHeaders(SendContext context)
    {
        SetHeader(context, Activity.Current?.GetBaggageItem(TelemetryConstants.Some), TelemetryConstants.Some);
    }

    private static void SetHeader(SendContext context, string? value, string name)
    {
        if (!string.IsNullOrWhiteSpace(value))
        {
            context.Headers.Set(name, value);
        }
    }
}
