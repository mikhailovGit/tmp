﻿using MassTransit;

namespace Common.Masstransit.Tracker.Filters;

public class TrackSendFilter<T> : IFilter<SendContext<T>> where T : class
{
    public Task Send(SendContext<T> context, IPipe<SendContext<T>> next)
    {
        HeaderTrackFilterExtension.SetAllHeaders(context);
        return next.Send(context);
    }

    public void Probe(ProbeContext context)
    {
    }
}
