﻿using System.Diagnostics;
using Common.Masstransit.Extensions;
using MassTransit;

namespace Common.Masstransit.Tracker.Filters;

public class TrackConsumeFilter<T> : IFilter<ConsumeContext<T>> where T : class
{
    public Task Send(ConsumeContext<T> context, IPipe<ConsumeContext<T>> next)
    {
        Activity.Current.EnrichActivity(context.Headers);

        return next.Send(context);
    }

    public void Probe(ProbeContext context)
    {
    }
}
