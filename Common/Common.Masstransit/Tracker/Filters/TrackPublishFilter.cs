﻿using System.Diagnostics;
using Common.Masstransit.Extensions;
using MassTransit;

namespace Common.Masstransit.Tracker.Filters;

public class TrackPublishFilter<T> : IFilter<PublishContext<T>> where T : class
{
    public Task Send(PublishContext<T> context, IPipe<PublishContext<T>> next)
    {
        HeaderTrackFilterExtension.SetAllHeaders(context);
        Activity.Current.EnrichActivity(context.Headers);
        return next.Send(context);
    }

    public void Probe(ProbeContext context)
    {
    }
}
