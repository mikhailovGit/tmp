﻿using Common.Masstransit.Tracker.Filters;
using MassTransit;

namespace Common.Masstransit.Tracker;

public static class TrackerExtensions
{
    public static void AddTrackerFilters(this IBusFactoryConfigurator configurator, IBusRegistrationContext context)
    {
        configurator.UseMessageScope(context);
        configurator.UseSendFilter(typeof(TrackSendFilter<>), context);
        configurator.UsePublishFilter(typeof(TrackPublishFilter<>), context);
        configurator.UseConsumeFilter(typeof(TrackConsumeFilter<>), context);
    }
}
