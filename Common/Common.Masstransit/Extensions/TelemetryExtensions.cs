﻿using System.Diagnostics;
using Common.Telemetry;
using MassTransit;

namespace Common.Masstransit.Extensions;

public static class Extensions
{
    public static void EnrichActivity(this Activity? activity, Headers headers)
    {
        var someValue = headers.Get<string>(TelemetryConstants.Some);

        activity?.AddTagIfNotExist(TelemetryConstants.Some, someValue);
        activity?.AddAddBaggageIfNotExist(TelemetryConstants.Some, someValue);
    }
}
