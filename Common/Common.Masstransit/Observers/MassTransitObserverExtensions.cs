﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Masstransit.Observers;

public static class MassTransitObserverExtensions
{
    /// <summary>
    ///     add a masstransit observer
    /// </summary>
    /// <param name="builder"></param>
    public static void AddMassTransitObserver(this WebApplicationBuilder builder)
    {
        builder.Services
            .AddTransient<ReceiveObserverLogger>()
            .AddTransient<PublishObserverLogger>()
            .AddTransient<SendObserverLogger>();
    }

    /// <summary>
    ///     Connect an masstransit observer
    /// </summary>
    /// <param name="configurator"></param>
    /// <param name="context"></param>
    public static void ConnectMassTransitObserver(
        this IBusFactoryConfigurator configurator,
        IBusRegistrationContext context)
    {
        configurator.ConnectReceiveObserver(context.GetRequiredService<ReceiveObserverLogger>());
        configurator.ConnectPublishObserver(context.GetRequiredService<PublishObserverLogger>());
        configurator.ConnectSendObserver(context.GetRequiredService<SendObserverLogger>());
    }
}
