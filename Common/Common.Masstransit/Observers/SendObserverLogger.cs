using System.Diagnostics;
using Common.Masstransit.Extensions;
using Common.Masstransit.Tracker.Filters;
using MassTransit;

namespace Common.Masstransit.Observers;

public class SendObserverLogger : BaseMasstransitObserver, ISendObserver
{
    public Task PreSend<T>(SendContext<T> context) where T : class
    {
        HeaderTrackFilterExtension.SetAllHeaders(context);
        Activity.Current.EnrichActivity(context.Headers);

        return Task.CompletedTask;
    }

    public Task PostSend<T>(SendContext<T> context) where T : class
    {
        return Task.CompletedTask;
    }

    public Task SendFault<T>(SendContext<T> context, Exception exception) where T : class
    {
        return Task.CompletedTask;
    }
}
