using System.Diagnostics;
using Common.Masstransit.Extensions;
using Common.Masstransit.Tracker.Filters;
using MassTransit;

namespace Common.Masstransit.Observers;

public class PublishObserverLogger : BaseMasstransitObserver, IPublishObserver
{
    public Task PrePublish<T>(PublishContext<T> context) where T : class
    {
        HeaderTrackFilterExtension.SetAllHeaders(context);
        Activity.Current.EnrichActivity(context.Headers);

        return Task.CompletedTask;
    }

    public Task PostPublish<T>(PublishContext<T> context) where T : class
    {
        return Task.CompletedTask;
    }

    public Task PublishFault<T>(PublishContext<T> context, Exception exception) where T : class
    {
        return Task.CompletedTask;
    }
}
