using System.Diagnostics;
using Common.Masstransit.Extensions;
using MassTransit;

namespace Common.Masstransit.Observers;

public class ReceiveObserverLogger : BaseMasstransitObserver, IReceiveObserver
{
    public Task PreReceive(ReceiveContext context)
    {
        Activity.Current.EnrichActivity(context.TransportHeaders);
        return Task.CompletedTask;
    }

    public Task PostReceive(ReceiveContext context)
    {
        return Task.CompletedTask;
    }

    public Task PostConsume<T>(ConsumeContext<T> context, TimeSpan duration, string consumerType) where T : class
    {
        return Task.CompletedTask;
    }

    public Task ConsumeFault<T>(
        ConsumeContext<T> context,
        TimeSpan duration,
        string consumerType,
        Exception exception) where T : class
    {
        Activity.Current.EnrichActivity(context.Headers);
        return Task.CompletedTask;
    }

    public Task ReceiveFault(ReceiveContext context, Exception exception)
    {
        return Task.CompletedTask;
    }
}
