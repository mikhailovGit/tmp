﻿using MassTransit;
using Saga.Engine.ExampleSaga;

namespace Saga.Host;

public static class StartupExtension
{
    public static void AddSagas(this IBusRegistrationConfigurator configurator)
    {
        configurator.SetInMemorySagaRepositoryProvider(); //можно хранить в бд, 

        configurator.AddSagaStateMachines(typeof(SimpleSagaStateMachine).Assembly);
    }
}
