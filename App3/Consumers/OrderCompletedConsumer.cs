﻿using App3.Extensions;
using Common.Masstransit.Events;
using MassTransit;

namespace App3.Consumers;

public class OrderCompletedConsumer : IConsumer<OrderCompleted.Raised>
{
    private readonly ILogger<OrderCompletedConsumer> _logger;

    public OrderCompletedConsumer(ILogger<OrderCompletedConsumer> logger)
    {
        _logger = logger;
    }

    public Task Consume(ConsumeContext<OrderCompleted.Raised> context)
    {
        _logger.OrderCompleted(context.Message.OrderId.ToString());
        return Task.CompletedTask;
    }
}
