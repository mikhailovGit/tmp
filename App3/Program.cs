using App3.Consumers;
using Common.Host.Extensions;
using Common.Telemetry;
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables();

builder.AddTracing("App3");
builder.AddLogging();

builder.Services.AddHttpContextAccessor();

builder.AddMasstransit(configurator => { configurator.AddConsumersFromNamespaceContaining<OrderCompletedConsumer>(); });

builder.Services.AddControllers();

var app = builder.Build();

app.UseHttpLogging();

app.MapControllers();

app.Run();
