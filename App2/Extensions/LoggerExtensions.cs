﻿namespace App2.Extensions;

public static class LoggerExtensions
{
    private static readonly Action<ILogger, string, Exception?> LogWithdrawMoney;

    static LoggerExtensions()
    {
        LogWithdrawMoney = LoggerMessage.Define<string>(
            LogLevel.Information, new EventId(1, nameof(WithdrawMoney)),
            "WithdrawMoney Completed (OrderId = '{OrderId}')");
    }

    public static void WithdrawMoney(this ILogger logger, string orderId)
    {
        if (logger.IsEnabled(LogLevel.Information))
        {
            LogWithdrawMoney(logger, orderId, null);
        }
    }
}
