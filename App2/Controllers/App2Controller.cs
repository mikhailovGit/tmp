using Common.Dto;
using Common.Masstransit.Events;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace App2.Controllers;

[ApiController]
[Route("[controller]")]
public class App2Controller : ControllerBase
{
    private readonly IPublishEndpoint _publishEndpoint;

    public App2Controller(IPublishEndpoint publishEndpoint)
    {
        _publishEndpoint = publishEndpoint;
    }

    [HttpPost]
    public async Task<IActionResult> PostAsync([FromBody] DtoExample dtoExample, CancellationToken cancellation)
    {
        await _publishEndpoint.Publish(
            new MasstransitEventExample
            {
                SomeString = dtoExample.SomeString
            }, cancellation);

        return Ok();
    }
}
