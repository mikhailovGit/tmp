﻿using App2.Extensions;
using Common.Enums;
using Common.Masstransit.Events;
using MassTransit;

namespace App2.Consumers;

public class WithdrawMoneyRequestedConsumer : IConsumer<WithdrawMoney.Requested>
{
    private readonly ILogger<WithdrawMoneyRequestedConsumer> _logger;

    public WithdrawMoneyRequestedConsumer(ILogger<WithdrawMoneyRequestedConsumer> logger)
    {
        _logger = logger;
    }

    public Task Consume(ConsumeContext<WithdrawMoney.Requested> context)
    {
        _logger.WithdrawMoney(context.Message.OrderId.ToString());

        return context.RespondAsync(new WithdrawMoney.Responded
        {
            CorrelationId = context.Message.CorrelationId,
            OrderId = context.Message.OrderId,
            OrderStatus = OrderStatus.WithdrawMoney
        });
    }
}
