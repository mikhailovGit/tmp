using App2.Consumers;
using Common.Host.Extensions;
using Common.Telemetry;
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHttpContextAccessor();

builder.AddTracing("App2");
builder.AddLogging();

builder.AddMasstransit(configurator => { configurator.AddConsumersFromNamespaceContaining<WithdrawMoneyRequestedConsumer>(); });

builder.Services.AddHttpClient("MyClient");

var app = builder.Build();
builder.Configuration.AddEnvironmentVariables();

app.UseHttpLogging();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.Run();
