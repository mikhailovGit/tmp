using App1;
using App1.Consumers;
using Common.Host.Extensions;
using Common.Telemetry;
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables();

var configurationSection = builder.Configuration.GetSection(nameof(Settings));
builder.Services.AddOptions<Settings>().Bind(configurationSection);

builder.AddTracing("App1");
builder.AddLogging();

builder.AddMasstransit(configurator => { configurator.AddConsumersFromNamespaceContaining<SubmitOrderConsumer>(); });

builder.Services.AddHttpClient("MyClient");

builder.Services.AddControllers();
builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseHttpLogging();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.Run();
