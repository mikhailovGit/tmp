﻿using App1.Extensions;
using Common.Masstransit.Events;
using MassTransit;

namespace App1.Consumers;

public class SubmitOrderConsumer : IConsumer<SubmitOrder.Requested>
{
    private readonly ILogger<SubmitOrderConsumer> _logger;

    public SubmitOrderConsumer(ILogger<SubmitOrderConsumer> logger)
    {
        _logger = logger;
    }

    public Task Consume(ConsumeContext<SubmitOrder.Requested> context)
    {
        _logger.OrderSubmitted(context.Message.OrderId.ToString());

        return context.RespondAsync(new SubmitOrder.Responded
        {
            CorrelationId = context.Message.CorrelationId,
            OrderId = context.Message.OrderId,
            OrderStatus = context.Message.OrderStatus
        });
    }
}
