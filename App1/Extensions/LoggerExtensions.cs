﻿namespace App1.Extensions;

public static class LoggerExtensions
{
    private static readonly Action<ILogger, string, Exception?> _logOrderCompleted;
    private static readonly Action<ILogger, string, Exception?> _logOrderSubmitted;

    static LoggerExtensions()
    {
        _logOrderCompleted = LoggerMessage.Define<string>(
            LogLevel.Information, new EventId(1, nameof(OrderCompleted)),
            "Order Completed (OrderId = '{OrderId}')");

        _logOrderSubmitted = LoggerMessage.Define<string>(
            LogLevel.Information, new EventId(2, nameof(OrderSubmitted)),
            "Order Submitted (OrderId = '{OrderId}')");
    }

    public static void OrderCompleted(this ILogger logger, string orderId)
    {
        if (logger.IsEnabled(LogLevel.Information))
        {
            _logOrderCompleted(logger, orderId, null);
        }
    }

    public static void OrderSubmitted(this ILogger logger, string orderId)
    {
        if (logger.IsEnabled(LogLevel.Information))
        {
            _logOrderSubmitted(logger, orderId, null);
        }
    }
}
