using Common.Dto;
using Common.Enums;
using Common.Masstransit.Events;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace App1.Controllers;

[ApiController]
[Route("[controller]")]
public class App1Controller : ControllerBase
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly Settings _options;
    private readonly IPublishEndpoint _publishEndpoint;

    public App1Controller(
        IHttpClientFactory httpClientFactory,
        IPublishEndpoint publishEndpoint,
        IOptions<Settings> options)
    {
        _httpClientFactory = httpClientFactory;
        _publishEndpoint = publishEndpoint;
        _options = options.Value;
    }

    [HttpGet]
    public async Task<IActionResult> GetAsync(string someString, CancellationToken cancellation)
    {
        HttpResponseMessage response;
        using (var httpClient = _httpClientFactory.CreateClient("MyClient"))
        {
            response = await httpClient.PostAsJsonAsync(new Uri(_options.App2ApplicationUrl, "App2"), new DtoExample
            {
                SomeString = someString
            }, cancellation);
        }

        response.EnsureSuccessStatusCode();
        return Ok();
    }

    [HttpGet("SubmitOrder")]
    public async Task<IActionResult> SubmitOrderAsync(CancellationToken cancellation)
    {
        await _publishEndpoint.Publish(
            new SubmitOrder.Requested
            {
                OrderId = Guid.NewGuid(),
                CorrelationId = Guid.NewGuid(),
                OrderStatus = OrderStatus.Submitted
            }, cancellation);

        return Ok();
    }
}
